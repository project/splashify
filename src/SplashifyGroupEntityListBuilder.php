<?php

namespace Drupal\splashify;

use Drupal\Core\Link;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Splashify group entity entities.
 *
 * @ingroup splashify
 */
class SplashifyGroupEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\splashify\Entity\SplashifyGroupEntity */
    $row['id'] = $entity->id();
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // Please manually remove the `use LinkGeneratorTrait;` statement from this class.
    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url(
      'entity.splashify_group_entity.edit_form', [
        'splashify_group_entity' => $entity->id(),
      ]
    ));
    return $row + parent::buildRow($entity);
  }

}
