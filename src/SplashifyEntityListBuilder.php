<?php

namespace Drupal\splashify;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;
use Drupal\splashify\Entity\SplashifyGroupEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Splashify entity entities.
 *
 * @ingroup splashify
 */
class SplashifyEntityListBuilder extends EntityListBuilder {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new SplashifyEntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $storage);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['group'] = $this->t('Group');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\splashify\Entity\SplashifyEntity */
    $row['id'] = $entity->id();
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // Please manually remove the `use LinkGeneratorTrait;` statement from this class.
    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url(
      'entity.splashify_entity.edit_form', [
        'splashify_entity' => $entity->id(),
      ]
    ));
    $entity_id = $entity->field_group->getValue();
    $entity_id = $entity_id[0]['target_id'];
    $entity_group = SplashifyGroupEntity::load($entity_id);
    if (!empty($entity_group)) {
      // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
      // Please manually remove the `use LinkGeneratorTrait;` statement from this class.
      $row['group'] = Link::fromTextAndUrl($entity_group->label(), new Url(
        'entity.splashify_group_entity.edit_form', [
          'splashify_group_entity' => $entity_id,
        ]
      ));
    }
    else {
      $row['group'] = $this->t('None');
    }

    $row['weight'] = $entity->field_weight->value;
    return $row + parent::buildRow($entity);
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->entityTypeManager->getStorage('splashify_entity')
      ->getQuery()
      ->accessCheck(TRUE)
      ->sort('field_weight', 'DESC')
      ->sort('id', 'DESC');
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
