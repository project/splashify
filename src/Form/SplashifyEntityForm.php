<?php

namespace Drupal\splashify\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Splashify entity edit forms.
 *
 * @ingroup splashify
 */
class SplashifyEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\splashify\Entity\SplashifyEntity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Splashify entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Splashify entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.splashify_entity.canonical', ['splashify_entity' => $entity->id()]);
    return $status;
  }

}
