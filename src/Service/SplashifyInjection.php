<?php
namespace Drupal\splashify\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\colorbox\ColorboxAttachment;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\splashify\Entity\SplashifyEntity;
use Drupal\splashify\Entity\SplashifyGroupEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SplashifyInjection.
 *
 * @package Drupal\splashify\Service
 */
class SplashifyInjection {

  protected $splash = NULL;

  /**
   * Default time line.
   */
  protected $timeLine = [
    // Set to expire in one year.
    'once' => 31536000,
    // Set to expire in 24 hours.
    'daily' => 86400,
    // Set to expire in 7 days.
    'weekly' => 604800,
    // Always when load page.
    'always' => 0,
  ];

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Colorbox attachment service.
   *
   * @var \Drupal\colorbox\ColorboxAttachment
   */
  protected ColorboxAttachment $colorboxAttachment;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Account proxy service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $accountProxy;

  /**
   * Patch matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected PathMatcherInterface $pathMatcher;

  /**
   * Current path stack service.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPathStack;

  /**
   * Path alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected AliasManagerInterface $aliasManager;

  /**
   * Request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new SplashifyInjection object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\colorbox\ColorboxAttachment $colorbox_attachement
   *   Colorbox attachment service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy service.
   * @param \Drupal\Core\Path\PathMatcherInterface $patch_matcher
   *   Patch matcher service.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path_stack
   *   Current path stack service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   Path alias manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ColorboxAttachment $colorbox_attachement,
    TimeInterface $time,
    AccountProxyInterface $account_proxy,
    PathMatcherInterface $patch_matcher,
    CurrentPathStack $current_path_stack,
    AliasManagerInterface $alias_manager,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->colorboxAttachment = $colorbox_attachement;
    $this->time = $time;
    $this->accountProxy = $account_proxy;
    $this->pathMatcher = $patch_matcher;
    $this->currentPathStack = $current_path_stack;
    $this->aliasManager = $alias_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Check is splash exist. If exist then remembered.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request object.
   *
   * @return bool
   *   The check result.
   */
  public function isSplashExist(?Request $request = NULL) {
    // At the first check in the request no session. It should pass this check.
    if (is_object($request) && !$request->hasSession()) {
      return TRUE;
    }

    if (!empty($this->splash)) {
      return TRUE;
    }

    $this->splash = $this->findSplash();

    return !empty($this->splash);
  }

  /**
   * Received all of splash groups that can be displayed on the current page.
   *
   * @return array
   *   Array of SplashifyGroupsEntity.
   */
  private function getPageGroups() {
    $groups = SplashifyGroupEntity::loadMultiple();

    $page_groups = [];

    foreach ($groups as $group) {
      if ($this->checkRole($group) && $this->checkWhere($group)) {
        $page_groups[$group->id()] = $group;
      }
    }

    return $page_groups;
  }

  /**
   * Received all of splashes that can be displayed on the current page.
   *
   * @return array
   *   Array of SplashifyEntity.
   */
  private function getSplashes() {
    $page_groups = $this->getPageGroups();
    $page_groups_id = array_keys($page_groups);

    if (empty($page_groups_id)) {
      return [];
    }

    $splashes_id = $this->entityTypeManager->getStorage('splashify_entity')
      ->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', 1)
      ->sort('field_weight', 'DESC')
      ->sort('id', 'DESC')
      ->condition('field_group', $page_groups_id, 'IN')
      ->execute();

    if (empty($splashes_id)) {
      return [];
    }

    $splashes = SplashifyEntity::loadMultiple($splashes_id);

    return $splashes;
  }

  /**
   * Choosing an entity which will be displayed.
   *
   * @return SplashifyEntity|null
   *   Splash that passes all conditions and has the biggest weight.
   */
  private function findSplash() {
    $splashes = $this->getSplashes();

    // Check which item need display.
    foreach ($splashes as $splash) {
      if ($this->checkFrequency($splash)) {
        return $splash;
      }
    }

    return NULL;
  }

  /**
   * Generated render element.
   *
   * @param $splash
   *   Splash which will be displayed.
   * @return array
   */
  private function getRenderElement($splash) {
    $build = [];
    $mode = $splash->getGroup()->getMode();

    switch ($mode) {
      case 'redirect':
        $build = [
          '#attached' => [
            'drupalSettings' => [
              'splashify' => [
                'mode' => 'redirect',
                'url' => '/splashify/' . $splash->id(),
              ],
            ],
            'library' => [
              'splashify/redirect',
            ],
          ],
        ];

        break;

      case 'window':
        $size = explode('x', $splash->getGroup()->getSize());
        $width = is_numeric($size[0]) ? $size[0] : 800;
        $height = is_numeric($size[1]) ? $size[1] : 600;

        $build = [
          '#attached' => [
            'drupalSettings' => [
              'splashify' => [
                'mode' => 'window',
                'url' => '/splashify/' . $splash->id(),
                'size' => "width={$width}, height={$height}",
              ],
            ],
            'library' => [
              'splashify/window',
            ],
          ],
        ];

        break;

      case 'full_screen':
        $build = [
          '#theme' => 'splashify',
          '#splashify_content' => $splash->getContent(),
          '#attached' => [
            'drupalSettings' => [
              'splashify' => [
                'mode' => 'full_screen',
              ],
            ],
            'library' => [
              'splashify/full_screen',
            ],
          ],
        ];
        break;

      case 'lightbox':
        $size = explode('x', $splash->getGroup()->getSize());
        $this->colorboxAttachment->attach($build);

        $build['#attached']['drupalSettings']['splashify'] = [
          'mode' => 'lightbox',
          'url' => '/splashify/' . $splash->id(),
          'width' => is_numeric($size[0]) ? $size[0] : 800,
          'height' => is_numeric($size[1]) ? $size[1] : 600,
        ];

        $build['#attached']['library'][] = 'splashify/lightbox';
        break;
    }

    $build['#cache']['max-age'] = 0;
    $build['#attached']['drupalSettings']['splashify']['referrer_check'] = !$splash->getGroup()
      ->isDisableReferrerCheck();

    $build['#attached']['library'] = $build['#attached']['library'] ?? [];

    array_unshift($build['#attached']['library'], 'splashify/splash_init');

    return $build;
  }

  /**
   * Returns render element.
   *
   * If this method is called it is considered that splash was shown.
   *
   * @return array
   */
  public function getAttach() {

    if (!$this->isSplashExist()) {
      return [];
    }

    setcookie("splashify[" . $this->splash->id() . "]", $this->time->getRequestTime(), NULL, '/');
    return $this->getRenderElement($this->splash);
  }

  /**
   * Check if the role of the current user pass the conditions in the group.
   *
   * @param $group
   * @return bool
   */
  private function checkRole($group) {
    // Get user account.
    $account = $this->accountProxy->getAccount();

    // Check whether use role setting is checked.
    if ($group->isRestrictRoles()) {

      $account_roles = $account->getRoles();
      $group_roles = $group->getRoles();

      return !empty(array_intersect($account_roles, $group_roles));
    }

    return TRUE;
  }

  /**
   * Check if the current page pass the conditions in the group.
   *
   * @param $group
   * @return bool
   */
  private function checkWhere($group) {
    $where = $group->getWhere();

    switch ($where) {
      case 'all':
        return TRUE;

      case 'home':
        $is_front = $this->pathMatcher->isFrontPage();
        return $group->isOpposite() ? !$is_front : $is_front;

      case 'list':
        $pages = mb_strtolower($group->getListPages());

        $path = $this->currentPathStack->getPath();
        // Do not trim a trailing slash if that is the complete path.
        $path = $path === '/' ? $path : rtrim($path, '/');
        $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));

        $is_match = $this->pathMatcher->matchPath($path_alias, $pages) ||
          (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));

        return $group->isOpposite() ? !$is_match : $is_match;
    }

    return TRUE;
  }

  /**
   * Checks can we display splash again from the group.
   *
   * @param $splash
   * @return bool
   */
  private function checkFrequency($splash) {
    $frequency = $splash->getGroup()->getOften();

    if ($frequency == 'never') {
      return FALSE;
    }

    $cookie = $this->requestStack->getCurrentRequest()->cookies;
    $splashify_cookies = $cookie->get('splashify');

    if (!is_array($splashify_cookies) || !array_key_exists($splash->id(), $splashify_cookies)) {
      return TRUE;
    }

    $expired_time = $this->time->getRequestTime() - $splashify_cookies[$splash->id()];
    return $expired_time >= $this->timeLine[$frequency];
  }

}
