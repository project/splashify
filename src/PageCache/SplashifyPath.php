<?php

namespace Drupal\splashify\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\splashify\Service\SplashifyInjection;
use Symfony\Component\HttpFoundation\Request;

/**
 * Disable cache if splash exist.
 */
class SplashifyPath implements RequestPolicyInterface {

  /**
   * Splashify Injection service.
   *
   * @var \Drupal\splashify\Service\SplashifyInjection
   */
  protected SplashifyInjection $splashifyInjection;

  /**
   * Kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected KillSwitch $killSwitch;

  /**
   * Constructs a new SplashifyPath object.
   *
   * @param \Drupal\splashify\Service\SplashifyInjection $splashify_injection
   *   Splashify Injection service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   Kill switch service.
   */
  public function __construct(SplashifyInjection $splashify_injection, KillSwitch $kill_switch) {
    $this->splashifyInjection = $splashify_injection;
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    if ($this->splashifyInjection->isSplashExist($request)) {
      $this->killSwitch->trigger();
      return static::DENY;
    }

    return static::ALLOW;
  }

}
